package id.ac.ui.cs.mobileprogramming.indraseptama.siontime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "MainActivity";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;

    private long mStartTimeInMillis;
    private long mTimeLeftInMillis;
    private boolean mLocationPermissionGranted = false;
    private boolean mTimerRunning;

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlacesClient placesClient;
    private EditText searchText;
    private EditText timeInput;
    private RelativeLayout relativeLayout1;
    private RelativeLayout relativeLayout2;
    private ProgressBar progressBar;
    private ImageView mGPS;
    private TextView mTextViewCountDown;
    private TextView mTextQuote;
    private Button mButton;
    private CountDownTimer mCountDownTimer;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchText = findViewById(R.id.searchEditText);
        timeInput = findViewById(R.id.timeEditText);
        mTextViewCountDown = findViewById(R.id.textViewCountDown);
        mButton = findViewById(R.id.buttonMulai);
        relativeLayout1 = findViewById(R.id.relative1);
        relativeLayout2 = findViewById(R.id.relative2);
        progressBar = findViewById(R.id.progresBar);
        mTextQuote = findViewById(R.id.textQuote);

        mTextQuote.setText(stringArrayFromJNI()[randomNumber()]);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTextQuote.setVisibility(View.INVISIBLE);
            }
        }, 5000);

        getLocationPermission();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(mLocationPermissionGranted){
            getDeviceLocation();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            init();
        }
    }

    private void init(){
        progressBar.setVisibility(View.INVISIBLE);
        mTextViewCountDown.setVisibility(View.INVISIBLE);

        String apiKey = getString(R.string.google_apps_api_key);
        mGPS = findViewById(R.id.ic_gps);

        if(!Places.isInitialized()){
            Places.initialize(getApplicationContext(), apiKey);
        }

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimerRunning) {
                    resetTimer();
                } else {
                    mTimeLeftInMillis = Long.parseLong(timeInput.getText().toString()) * 60000;
                    startTimer();
                    sendOnChannel2();
                }
            }
        });

        placesClient = Places.createClient(this);

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        || event.getAction() == KeyEvent.KEYCODE_ENTER){
                    geoLocate();
                }
                return false;
            }
        });

        mGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });

        hideSoftKeyboard();
    }

    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MainActivity.this);
    }

    private void getLocationPermission(){
        String[] permissions = {FINE_LOCATION, COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mLocationPermissionGranted = true;
                initMap();
            }
            else{
                ActivityCompat.requestPermissions(this, permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
        else{
            ActivityCompat.requestPermissions(this, permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private void getDeviceLocation(){
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try{
            if(mLocationPermissionGranted){
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            Location currentLocation = (Location) task.getResult();
                            zoomCamera(new LatLng(currentLocation.getLatitude(),
                                            currentLocation.getLongitude()), DEFAULT_ZOOM,
                                    "Lokasi Anda Saat Ini");
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.e(TAG, "getDeviceLocation: SecurityException " +  e.getMessage());
        }
    }

    private void zoomCamera(LatLng latLng, float zoom, String title){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        if(title!="Lokasi Anda Saat Ini"){
            MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(title);
            mMap.clear();
            mMap.addMarker(markerOptions);
        }
        hideSoftKeyboard();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0){
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                }
                mLocationPermissionGranted = true;
                initMap();
        }
    }

    private void geoLocate(){
        if(isNetworkConnected()){
            String searchString = searchText.getText().toString();
            Geocoder geocoder = new Geocoder(MainActivity.this);
            List<Address> addressList = new ArrayList<>();
            try{
                addressList = geocoder.getFromLocationName(searchString, 1);
            }catch (IOException e){
                Log.d(TAG, "geoLocate: IOException: " + e.getMessage());
            }

            if(addressList.size()>0){
                Address address = addressList.get(0);
                zoomCamera(new LatLng(address.getLatitude(), address.getLongitude()),
                        DEFAULT_ZOOM,
                        address.getAddressLine(0));
                Log.d(TAG, "geoLocate: found location" + address.toString());
            }
        }
        else{
            Toast.makeText(this, "Aktifkan jaringan anda", Toast.LENGTH_SHORT).show();
        }
    }

    private void hideSoftKeyboard(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void startTimer() {
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                mTimerRunning = false;
                mButton.setText("Start");
            }
        }.start();

        mTimerRunning = true;
        relativeLayout1.setVisibility(View.INVISIBLE);
        relativeLayout2.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        mTextViewCountDown.setVisibility(View.VISIBLE);
        mButton.setText("batal");
    }

    private void resetTimer() {
        mCountDownTimer.cancel();
        mTimeLeftInMillis = mStartTimeInMillis;
        updateCountDownText();
        mTimerRunning = false;
        mButton.setText("mulai");
        relativeLayout1.setVisibility(View.VISIBLE);
        relativeLayout2.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        mTextViewCountDown.setVisibility(View.INVISIBLE);
    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        mTextViewCountDown.setText(timeLeftFormatted);
    }

    private void sendOnChannel2() {
        final int progressMax = (int) mTimeLeftInMillis;
        final NotificationHelper notificationHelper = new NotificationHelper(this);
        final NotificationCompat.Builder builder = notificationHelper.getChannel1Notification();
        notificationHelper.getNotificationManager().notify(2, builder.build());

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int progress = progressMax; progress >= 0; progress -= 1000) {
                    if(mTimerRunning) {
                        builder.setProgress(progressMax, progress, false);
                        builder.setOnlyAlertOnce(true);
                        builder.setPriority(NotificationCompat.PRIORITY_LOW);
                        notificationHelper.getNotificationManager().notify(2, builder.build());
                        SystemClock.sleep(1000);
                    }
                    else{
                        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancel(2);
                    }
                }
            }
        }).start();
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native String[] stringArrayFromJNI();
    public native int randomNumber();
}
