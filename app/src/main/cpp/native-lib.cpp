#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_id_ac_ui_cs_mobileprogramming_indraseptama_siontime_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
extern "C"
JNIEXPORT jobjectArray JNICALL
Java_id_ac_ui_cs_mobileprogramming_indraseptama_siontime_MainActivity_stringArrayFromJNI(
        JNIEnv *env, jobject thiz) {
    // TODO: implement stringArrayFromJNI()
    char *greetings[]={"Semoga Tepat Waktu!",
                  "Be On Time!",
                  "Waktu adalah segalanya",
                  "Sudah jam berapa sekarang?",
                  "Ayo produktif",
                  "Selesaikan satu satu"};

    jstring str;
    jobjectArray greeting = 0;
    jsize len = 6;
    int i;

    greeting = env->NewObjectArray(len,env->FindClass("java/lang/String"),0);

    for(i=0;i<6;i++)
    {
        str = env->NewStringUTF(greetings[i]);
        env->SetObjectArrayElement(greeting,i,str);
    }

    return greeting;
}

extern "C"
JNIEXPORT jint JNICALL
Java_id_ac_ui_cs_mobileprogramming_indraseptama_siontime_MainActivity_randomNumber(JNIEnv *env,
                                                                                   jobject thiz) {
    // TODO: implement randomNumber()
    int n;
    n = rand()%6 + 1;

    return n;
}